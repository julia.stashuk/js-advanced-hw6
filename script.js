//Асинхронність в JavaScript означає, що код може виконуватись паралельно, не очікуючи завершення попередніх операцій.

const btn = document.querySelector(".button");
const wrapper = document.querySelector(".wrapper");
async function getIP() {
  const resIP = await fetch("https://api.ipify.org/?format=json");
  const ip = await resIP.json();

  const ipString = ip.ip.toString();
  console.log(ipString);
  const resMyIP = await fetch(
    `http://ip-api.com/json/${ipString}?fields=1066525`
  );
  const myIp = await resMyIP.json();
  console.log(myIp);

  const ipHTML = document.createElement("div");
  ipHTML.classList.add("ip__content");
  ipHTML.innerHTML = `
    <h1>А ось і Ви!</h1>
    <h2>Континент: ${myIp.continent}</h2>
    <h3>Країна: ${myIp.country}</h3>
    <h4>Регіон: ${myIp.regionName}</h4>
    <p>Місто: ${myIp.city}</p>
    `;
  ipHTML.remove();
  wrapper.append(ipHTML);
}

btn.addEventListener("click", getIP);

